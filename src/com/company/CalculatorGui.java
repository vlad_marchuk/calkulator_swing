package com.company;
import  java.awt.*;
import  java.awt.event.*;
import  javax.swing.*;

public class CalculatorGui extends JFrame {
    public JTextField inputFirstNum = new JTextField(20);
    public JButton buttonPlus = new JButton("   +   ");
    public JButton buttonMinus = new JButton("   -   ");
    public JButton buttonMultiply = new JButton("   *   ");
    public JButton buttonDivide = new JButton("    /   ");
    public JButton buttonResult = new JButton("                            RESULT                     ");
    public JTextField inputSecondNum = new JTextField(20);
    public JLabel labelFirst = new JLabel("        Число 1     ");
    public JLabel labelSecond = new JLabel("       Число 2     ");
    public JLabel labelResult = new JLabel("Результат");

    Service service = new Service();

    public CalculatorGui() {
        super("Calculator_Terminator");
        this.setBounds(500, 500, 280, 250);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setBackground(Color.lightGray);
        container.setLayout(new FlowLayout());

        container.add(labelFirst);
        container.add(inputFirstNum);
        container.add(labelSecond);
        container.add(inputSecondNum);
        container.add(buttonPlus);
        container.add(buttonMinus);
        container.add(buttonMultiply);
        container.add(buttonDivide);
        container.add(buttonResult);
        container.add(labelResult);
        buttonPlus.addActionListener(service);
        buttonMinus.addActionListener(service);
        buttonMultiply.addActionListener(service);
        buttonDivide.addActionListener(service);
        buttonResult.addActionListener(service);
    }

    char operation;
    double result;
    double num1;
    double num2;
    String resultLine;

    class Service implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                num1 = Double.parseDouble(inputFirstNum.getText());
                num2 = Double.parseDouble(inputSecondNum.getText());
                if (e.getSource() == buttonPlus) {
                    operation = '+';
                    result = num1 + num2;
                }
                if (e.getSource() == buttonMinus) {
                    operation = '-';
                    result = num1 - num2;
                }
                if (e.getSource() == buttonMultiply) {
                    operation = '*';
                    result = num1 * num2;
                }
                if (e.getSource() == buttonDivide) {
                    operation = '/';
                    result = num1 / num2;
                }
                ///////////////////////////////////////////////
                if (e.getSource() == buttonResult) {
                    resultLine =""+ num1 + " " + operation + " "+ num2 + " = "+ result;
                    labelResult.setText(resultLine);
                    inputFirstNum.setText("");
                    inputSecondNum.setText("");
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Введите в поле число");
            }
        }
    }
}
